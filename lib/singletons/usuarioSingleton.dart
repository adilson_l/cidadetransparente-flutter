

import 'package:cidadetransparente/classes/usuario.dart';

class UsuarioSing{

  static UsuarioSing _instance;
  Usuario usuario;

  UsuarioSing._construtor(this.usuario);

  factory UsuarioSing({Usuario usuario}){
    _instance ??=UsuarioSing._construtor(usuario);
    return _instance;
  }

}