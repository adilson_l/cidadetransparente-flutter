import 'package:cloud_firestore/cloud_firestore.dart';

class Problemas{
  String descricao;
  String tipo;
  String endereco;
  Timestamp data;
  String status;
  String orgao;
  String solucao;
  String usuarioId;
  GeoPoint ponto;
  List  fotos;
  String id;

  Problemas(this.descricao,this.tipo,this.data,this.orgao,this.solucao,this.status,this.usuarioId,this.fotos,this.ponto,this.endereco,this.id);

  Problemas.map(Map<String, dynamic> dados,String _id){
    descricao=dados['descricao'];
    tipo=dados['tipo'];
    ponto=dados['ponto'];
    data=dados['data'];
    orgao=dados['orgao'];
    solucao=dados['solucao'];
    endereco=dados['endereco'];
    fotos=dados['fotos'];
    status=dados['status'];
    usuarioId=dados['usuarioId'];
    id=_id;
  }

  Map<String,dynamic> toMap(){
    return <String,dynamic>{
      'descricao':descricao,
      'tipo':tipo,
      'ponto':ponto,
      'data':data,
      'orgao':orgao,
      'solucao':solucao,
      'status':status,
      'usuarioId':usuarioId,
      'endereco':endereco,
      'fotos':fotos
    };
  }
}
