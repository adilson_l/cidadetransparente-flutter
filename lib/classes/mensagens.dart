

import 'package:cloud_firestore/cloud_firestore.dart';

class Mensagem{

  String usuarioId;
  String nome;
  Timestamp data;
  String mensagem;

  Mensagem(this.usuarioId,this.data,this.mensagem,this.nome);

  Mensagem.map(Map<String,dynamic> dados){
    usuarioId=dados['usuarioId'];
    data=dados['data'];
    mensagem=dados['mensagem'];
    nome=dados['nome'];
  }

  Map<String,dynamic> toMap(){
    return <String,dynamic>{
      'usuarioId':usuarioId,
      'data':data,
      'mensagem':mensagem,
      'nome':nome
    };
  }

}