class Agencia{

  String nome;
  String endereco;
  String telefone;
  String email;
  String id;

  Agencia(this.nome,this.email,this.telefone,this.endereco);

  Agencia.map(Map<String,dynamic> dados,String id){
    nome=dados['nome'];
    endereco=dados['endereco'];
    telefone=dados['telefone'];
    email=dados['email'];
    id=dados['id'];
  }

  Map<String,dynamic> toMap(){
    return <String,dynamic>{
      'nome':nome,
      'endereco':endereco,
      'telefone':telefone,
      'email':email,
    };
  }

}