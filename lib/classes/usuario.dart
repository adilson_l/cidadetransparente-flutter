class Usuario{

  String email;
  String nome;
  String sexo;
  double idade;
  String id;
  String uid;

  Usuario(this.email,this.nome,this.sexo,this.idade,this.id,this.uid);

  Usuario.map(Map<String, dynamic> dados, String _id){
    email=dados['email'];
    nome=dados['nome'];
    sexo=dados['sexo'];
    idade=dados['idade'];
    uid=dados['uid'];
    id=_id;
  }

  Map<String,dynamic> toMap(){
    return <String,dynamic>{
      'email':email,
      'nome':nome,
      'sexo':sexo,
      'idade':idade,
      'uid':uid
    };
    
  }
}
