import 'package:cached_network_image/cached_network_image.dart';
import 'package:cidadetransparente/classes/problema.dart';
import 'package:cidadetransparente/components/popUp.dart';
import 'package:cidadetransparente/pages/chatPage.dart';
import 'package:cidadetransparente/singletons/usuarioSingleton.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class DetalharProblemaPage extends StatefulWidget {
  DetalharProblemaPage(this.problemas,this.ponto);
  final Problemas problemas;
  final CameraPosition ponto;
  @override
  _DetalharProblemaPageState createState() => _DetalharProblemaPageState();
}

class _DetalharProblemaPageState extends State<DetalharProblemaPage> {
  Set<Marker> markes = Set();
  UsuarioSing usuarioSing = UsuarioSing();
  @override
  void initState() {
    markes.add(Marker(markerId: MarkerId('value'),position: LatLng(widget.problemas.ponto.latitude,widget.problemas.ponto.longitude)));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.problemas.tipo),
        centerTitle: true,
        elevation: 0,
      ),
      floatingActionButton: widget.problemas.usuarioId==usuarioSing.usuario.id ?
       FloatingActionButton(child: Icon(Icons.info),onPressed: (){
         showDialog(
           context: context,
           builder: (context)=>Dialog(
             child:SingleChildScrollView(
               child:  AlteraEstadoProblema(widget.problemas),
             ),
           )
         );
       },):null,
      body: Stack(
        children: <Widget>[
          Container(
              height:  MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Colors.green[200],
                    Colors.green
                  ],
                ),
              ),
            ),
          SingleChildScrollView(
            child:Padding(
              padding: EdgeInsets.all(8),
              child:  Column(
              children: <Widget>[
                 Center(
                          child: Text('Status:'+widget.problemas.status,style: TextStyle(fontSize: 20,color: Colors.white),),
                        ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 8),
                  ),
               Center(
                 child:  Container(
                  width: MediaQuery.of(context).size.width*0.8,
                  height: MediaQuery.of(context).size.height*0.3,
                  child: GoogleMap(
                    markers: markes,
                    mapType: MapType.hybrid,
                    initialCameraPosition: widget.ponto,
                  ),
                ),
               ),
               Container(
                 width: MediaQuery.of(context).size.width*0.83,
                  height: MediaQuery.of(context).size.height*0.2,
                  child: Card(
                    color: Colors.lightGreen,
                    child: Column(
                      children: <Widget>[
                        Center(
                          child: Text("Descrição",style: TextStyle(fontSize: 20,color: Colors.white),),
                        ),
                        Flexible(
                          child: Text(widget.problemas.descricao,style:TextStyle(color: Colors.white)),
                        )
                      ],
                    ),
                  ),
               ),
                Center(
                          child: Text("Galeria",style: TextStyle(fontSize: 20,color: Colors.white),),
                        ),
               widget.problemas.fotos.isEmpty ?
               Text('Não há fotos',style: TextStyle(color: Colors.white),):
              Container(
                height: MediaQuery.of(context).size.height*0.2,
                width: MediaQuery.of(context).size.width*0.83,
                child:  Card(
                  color: Colors.lightGreen,
                  child: ListView.builder(
                 shrinkWrap: true,
                 scrollDirection: Axis.horizontal,
                 itemCount: widget.problemas.fotos.length,
                 itemBuilder: (context,i){
                   return Padding(
                     padding: EdgeInsets.all(8),
                     child: CachedNetworkImage(
                     imageUrl: widget.problemas.fotos[i],
                     placeholder: (context, url) => new CircularProgressIndicator(),
                     errorWidget: (context, url, error) => new Icon(Icons.error),
                   ),
                   );
                 },

               ),
                )
              ),
              RaisedButton(
                child: Text("Comentários"),
                onPressed: (){
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (context)=>ChatPage(widget.problemas))
                  );
                },
               )
               
              ],
            ),
            ),
          )
        ],
      ),
    );
  }
}