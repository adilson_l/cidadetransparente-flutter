import 'package:cidadetransparente/classes/problema.dart';
import 'package:cidadetransparente/pages/detalharProblema.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ListarProblemaPage extends StatefulWidget {
  @override
  _ListarProblemaPageState createState() => _ListarProblemaPageState();
}

class _ListarProblemaPageState extends State<ListarProblemaPage> {

  List<Problemas> problemas= List();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Listar problemas"),
        centerTitle: true,
        elevation: 0,
      ),
      body: Stack(
        children: <Widget>[
          Container(
              height:  MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Colors.green[200],
                    Colors.green
                  ],
                ),
              ),
            ),
            StreamBuilder(
              stream: Firestore.instance.collection('problemas').snapshots(),
              builder: (context,snap){
               switch (snap.connectionState) {
                 case ConnectionState.waiting:
                  return CircularProgressIndicator();                   
                   break;
                  case ConnectionState.none:
                    return Text("Não há dados");
                    break;
                 default:
                 if(problemas.isNotEmpty)
                  problemas.clear();
                 snap.data.documents.forEach((f){
                   problemas.add(Problemas.map(f.data,f.documentID));
                 });
                 print(problemas);
                 return ListView.builder(
                   itemCount: problemas.length,
                   itemBuilder:(context, i){
                     return Card(
                       margin: EdgeInsets.all(8),
                       color: Colors.lightGreen,
                       child: Column(
                         children: <Widget>[
                           Text(problemas[i].tipo,style:TextStyle(fontSize: 20,color: Colors.white)),
                           Row(
                             mainAxisAlignment: MainAxisAlignment.center,
                             children: <Widget>[
                               Text("Status: ",style: TextStyle(color: Colors.white),),
                               Text(problemas[i].status,style: TextStyle(color: Colors.white),)
                             ],
                           ),
                              Row(
                             mainAxisAlignment: MainAxisAlignment.center,
                             children: <Widget>[
                               Text("Endereco: ",style: TextStyle(color: Colors.white),),
                               Text(problemas[i].endereco,style: TextStyle(color: Colors.white),)
                             ],
                           ),
                          FlatButton(
                                child: Text("Ver mais..",style: TextStyle(color: Colors.white),),
                                onPressed: (){
                                  CameraPosition ponto = CameraPosition(
                                    target: LatLng(problemas[i].ponto.latitude,problemas[i].ponto.longitude),
                                    zoom: 15
                                  );
                                  Navigator.of(context).push(MaterialPageRoute(
                                    builder:(context)=>DetalharProblemaPage(problemas[i],ponto)
                                  ));
                                },
                              )
                         ],
                       ),
                     );
                   },
                 );
               }
           
                
              },
            )
        ],
      ),
    );
  }
}