import 'package:cidadetransparente/classes/mensagens.dart';
import 'package:cidadetransparente/classes/problema.dart';
import 'package:cidadetransparente/singletons/usuarioSingleton.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ChatPage extends StatefulWidget { 
  ChatPage(this.problemas);
  final Problemas problemas;
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  UsuarioSing  singleton= UsuarioSing();
  TextEditingController msgController=TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Comentários"),
        centerTitle: true,
        elevation: 0,
      ),
      body: Stack(
        children: <Widget>[
           Container(
              height:  MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Colors.green[200],
                    Colors.green
                  ],
                ),
              ),
            ),
          Container(
            height: MediaQuery.of(context).size.height*0.8,
            child: Column(
              children: <Widget>[
                Expanded(
                  child: StreamBuilder(
                stream:   Firestore.instance.collection("problemas/${widget.problemas.id}/chat").snapshots(),
                builder: (context,snap){
                      switch (snap.connectionState) {
                        case ConnectionState.waiting:
                             return CircularProgressIndicator();                   
                         break;
                       case ConnectionState.none:
                             return Text("Não há dados");
                          break;
                        default:
                        return ListView.builder(
                          shrinkWrap: true,
                            itemCount: snap.data.documents.length,
                            itemBuilder: (context,i){
                              DateTime hora = snap.data.documents[i].data["data"].toDate();
                              return Container(
                                margin: EdgeInsets.symmetric(horizontal: 5),
                                padding: EdgeInsets.symmetric(vertical: 5),
                                alignment: snap.data.documents[i]['usuarioId'] == singleton.usuario.id?
                                Alignment.centerRight : Alignment.centerLeft,
                                child: Card(
                                  elevation: 2,
                                  color: snap.data.documents[i]['usuarioId'] == singleton.usuario.id?
                                  Colors.blue : Colors.lightBlue,
                                  child: Wrap(
                                        alignment: WrapAlignment.end,
                                        children: <Widget>[
                                          Padding(
                                            child:   Text(snap.data.documents[i].data['mensagem'],
                                          style: TextStyle(color: Colors.white, 
                                            fontSize: 16),),
                                            padding: EdgeInsets.all(8),
                                          ),
                                          Text(snap.data.documents[i].data["nome"]+" "+hora.hour.toString()+":"+hora.minute.toString(),
                                          style: TextStyle(color: Colors.white, 
                                            fontSize: 10),),
                                        ],

                                  ),
                                ),
                              );
                            },
                        )
                        ;

                      }
                },              
            ),
                ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child:   TextField(
              controller: msgController,
              decoration: InputDecoration(
                hintText: "Digite algo...",
                hintStyle: TextStyle(color: Colors.white),
                suffixIcon: IconButton(
                  icon: Icon(Icons.send,color: Colors.white,),
                  onPressed: (){
                    if(msgController.text.isNotEmpty){
                        Mensagem msg = Mensagem(singleton.usuario.id, Timestamp.now(), msgController.text,singleton.usuario.nome);
                        Firestore.instance.collection("problemas/${widget.problemas.id}/chat").add(msg.toMap());
                        msgController.clear();
                    }
                  
                  },
                )
              ),
            ),
          )
              ],
            ) 
          ),
            
        ],
      ),
    );
  }
}