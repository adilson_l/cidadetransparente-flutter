import 'package:cidadetransparente/singletons/usuarioSingleton.dart';
import 'package:flutter/material.dart';

class MeuPerfilPage extends StatefulWidget {
  @override
  _MeuPerfilPageState createState() => _MeuPerfilPageState();
}

class _MeuPerfilPageState extends State<MeuPerfilPage> {
  UsuarioSing singleton= UsuarioSing();
  
  TextEditingController emailController = TextEditingController();
  TextEditingController nomeController = TextEditingController();
  TextEditingController idadeController = TextEditingController();
    TextEditingController sexoController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  void initState(){
    emailController = TextEditingController(text: singleton.usuario.email);
    nomeController = TextEditingController(text: singleton.usuario.nome);
    idadeController = TextEditingController(text: singleton.usuario.idade.toString());
    sexoController = TextEditingController(text: singleton.usuario.sexo);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Perfil"),
        centerTitle: true,
        elevation: 0,
      ),
      body: Stack(
        children: <Widget>[
          Container(
              height:  MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                  colors: [
                    Colors.green[200],
                    Colors.green
                  ],
                ),
              ),
            ),
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.all(16),
              child:Form(
                key: _formKey,
                child:  Column(
                children: <Widget>[
                TextField(
                   style: TextStyle(color: Colors.white),
                   enabled: false,
                  controller: emailController,
                  decoration: InputDecoration(
                    labelStyle: TextStyle(color: Colors.white),
                    labelText: "Email",
                  ),
                ),
                  TextField(
                    controller: nomeController,
                    enabled: false,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration( 
                    labelStyle: TextStyle(color: Colors.white),
                    labelText: "Nome",
                    ),
                  ),
                  TextField(
                    controller: idadeController,
                    enabled: false,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration( 
                    labelStyle: TextStyle(color: Colors.white),
                    labelText: "Idade",
                    ),
                  ),
                  TextField(
                    controller: sexoController,
                    enabled: false,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration( 
                    labelStyle: TextStyle(color: Colors.white),
                    labelText: "Sexo",
                    ),
                  )

                ],
              ),
              )
            )
          )
        ],
      ),
    );
  }
}