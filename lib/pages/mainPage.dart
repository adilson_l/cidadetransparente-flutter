import 'dart:async';

import 'package:cidadetransparente/DAO/problemaDAO.dart';
import 'package:cidadetransparente/components/gaveta.dart';
import 'package:cidadetransparente/pages/detalharProblema.dart';
import 'package:cidadetransparente/pages/listarProblemas.dart';
import 'package:cidadetransparente/singletons/usuarioSingleton.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  Completer<GoogleMapController> _controller = Completer();
  UsuarioSing usu = UsuarioSing();
Position position ;
Set<Marker> markes = Set();
 Future _goToPoint(CameraPosition cameraPosition) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
  }
  @override
  void initState(){
    super.initState();
      Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high).then((posicao){
        position=posicao;
        CameraPosition cameraPosition = CameraPosition(
          target: LatLng(posicao.latitude, posicao.longitude),
          zoom: 13,
          tilt: 50,
          
        );
/*         markes.add(Marker(position:  LatLng(posicao.latitude, posicao.longitude),markerId: MarkerId(''),

        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueMagenta),
        infoWindow: InfoWindow(title: "Titulo"),visible: true)); */
      _goToPoint(cameraPosition);
      setState(() {});
      });
    ProblemaDAO.buscarMarcadores().then((lista){
        if(lista!=null && lista.isNotEmpty){
          lista.forEach((f){
            double valor;
            switch (f.tipo) {
              case 'Água e Esgoto' :
                valor=BitmapDescriptor.hueAzure;
                break;
               case 'Árvores Caídas' :
                valor=BitmapDescriptor.hueGreen;
                break;
               case 'Assaltos' :
                valor=BitmapDescriptor.hueRed;
                break;
                 case 'Bueiros entupidos' :
                valor=BitmapDescriptor.hueViolet;
                break;
                 case 'Iluminação Pública' :
                valor=BitmapDescriptor.hueOrange;
                break;
                case 'Semáforos Queimados' :
                valor=BitmapDescriptor.hueYellow;
                break;
              default:
              valor=BitmapDescriptor.hueBlue;
            }
            markes.add(
              Marker(
                position: LatLng(f.ponto.latitude,f.ponto.longitude),
               markerId: MarkerId(f.id),
               infoWindow: InfoWindow(title: f.tipo),
               icon: BitmapDescriptor.defaultMarkerWithHue(valor),
               onTap: (){
                 CameraPosition ponto=CameraPosition(
                   target: LatLng(f.ponto.latitude,f.ponto.longitude),
                   zoom: 15
                 );
                 Navigator.of(context).push(MaterialPageRoute(builder: (context)=>DetalharProblemaPage(f, ponto)));
               }
               ));
          });
        }
        setState(() {});
    });
  }
  static final CameraPosition _pontoInicial = CameraPosition(
    target: LatLng(-19.7507844,  -47.936043),
    zoom: 13,
  );
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child:  Scaffold(
      appBar: AppBar(
        title: Text("Mapa"),
        centerTitle: true,
      ),
      drawer:Gaveta(),
      
      body: Center(
        child:GoogleMap(
          markers:markes ,
        mapType: MapType.hybrid,
        initialCameraPosition: _pontoInicial,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
      ),
    ),
    );
  }
}