import 'package:cidadetransparente/classes/agencia.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AgenciaPage extends StatefulWidget {
  @override
  _AgenciaPageState createState() => _AgenciaPageState();
}

class _AgenciaPageState extends State<AgenciaPage> {
  List<Agencia> agencias = new List();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Agências"),
        centerTitle: true,
        elevation: 0,
      ),
      body: Stack(
        children: <Widget>[
          Container(
              height:  MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                  colors: [
                    Colors.green[200],
                    Colors.green
                  ],
                ),
              ),
            ),
          StreamBuilder(
            stream: Firestore.instance.collection('agencias').snapshots(),
            builder: (context,snap){  
              switch (snap.connectionState) {
                  case ConnectionState.waiting:
                      return CircularProgressIndicator();                   
                    break;
                  case ConnectionState.none:
                      return Text("Não há dados");
                   break;
                default:
                if(agencias.isNotEmpty)
                  agencias.clear();
                snap.data.documents.forEach((f){
                  agencias.add(Agencia.map(f.data, f.documentID));
                });
                return ListView.builder(
                  itemCount: agencias.length,
                  itemBuilder: (context,i){
                    return Card(
                      color: Colors.lightGreen,
                      child: Column(
                        children: <Widget>[
                           Center(
                          child: Text(agencias[i].nome,style: TextStyle(fontSize: 20,color: Colors.white),),
                        ),
                         Row(
                             mainAxisAlignment: MainAxisAlignment.center,
                             children: <Widget>[
                               Text("Endereco: ",style: TextStyle(color: Colors.white),),
                              Flexible(child:  Text(agencias[i].endereco,style: TextStyle(color: Colors.white),),)
                             ],
                           ),
                        FlatButton(
                          child: Text(agencias[i].email,style: TextStyle(color: Colors.white),),
                          onPressed:()async{
                            _moveTo("mailto:<"+agencias[i].email+">");
                          }
                        ),
                        FlatButton(
                          child: Text(agencias[i].telefone,style: TextStyle(color: Colors.white),),
                          onPressed:()async{
                            _moveTo("tel:<"+agencias[i].telefone+">");
                          }
                        )
                        ],
                      ),
                      
                    );
                  },
                );
              }
            },
          )
        ],
      ),
    );
  }
  Future<void> _moveTo(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

}