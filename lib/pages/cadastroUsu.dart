import 'package:cidadetransparente/DAO/usuarioDAO.dart';
import 'package:cidadetransparente/classes/usuario.dart';
import 'package:cidadetransparente/components/textfieldpadrao.dart';
import 'package:cidadetransparente/pages/mainPage.dart';
import 'package:cidadetransparente/singletons/usuarioSingleton.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class CadastroUsuarioPage extends StatefulWidget {
  @override
  _CadastroUsuarioPageState createState() => _CadastroUsuarioPageState();
}

class _CadastroUsuarioPageState extends State<CadastroUsuarioPage> {
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  TextEditingController nomeController = TextEditingController();
  TextEditingController senhaController = TextEditingController();
  TextEditingController idadeController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  FirebaseUser user;
  double idade=0;
  String sexo="Masculino";
  bool fem=false;
  bool masc=true;
  bool versenha=false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cadastro"),
        centerTitle: true,
        elevation: 0,
      ),
      body:Builder(
        builder: (context)=>
         Stack(
        children: <Widget>[
          Container(
              height:  MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Colors.green[200],
                    Colors.green
                  ],
                ),
              ),
            ),
          SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
              ),
              TextFieldPadrao(
                labelText: "Nome",
                controller: nomeController,
                validator: (value){
                  if(value==null || value.isEmpty)
                    return "Campo obrigátorio";
                },
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
              ),
              TextFieldPadrao(
                labelText: "Email",
                keyboardType: TextInputType.emailAddress,
                controller: emailController,
                validator: (value){
                  if(value==null || value.isEmpty)
                    return "Campo obrigátorio";
                },
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
              ),
              TextFieldPadrao(
                labelText: "Senha",
                obscureText: versenha,
                suffix: IconButton(icon: Icon(versenha ?Icons.visibility: Icons.visibility_off,color: Colors.white,),
                onPressed: (){
                  setState(() {
                   versenha=!versenha; 
                  });
                },),
                controller: senhaController,
                validator: (value){
                  if(value==null || value.isEmpty)
                    return "Campo obrigátorio";
                },
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
              ),
             TextFieldPadrao(
                labelText: "Idade",
                keyboardType: TextInputType.number,
                controller: idadeController,
                validator: (value){
                  if(value==null || value.isEmpty)
                    return "Campo obrigátorio";
                },
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Radio(
                    groupValue: sexo,
                    value: "Feminino",
                    onChanged: (newValue){
                      setState(() {
                        fem=!fem;
                        masc=!masc;
                        sexo=newValue;
                      });
                    },
                  ),
                  Text("Feminino",style: TextStyle(color: Colors.white,fontSize: 24),),
                  Radio(
                    groupValue: sexo,
                    value:"Masculino",
                    onChanged: (newValue){
                      setState(() {
                        masc=!masc;
                        fem=!fem;
                        sexo=newValue;
                      });
                    },
                  ),
              Text("Masculino",style: TextStyle(color:Colors.white,fontSize: 24),),
                ],
              ),
              RaisedButton(
                 shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                child: Text("Cadastrar"),
                onPressed: ()async{
                  if(_formKey.currentState.validate()){
                    
                      Scaffold.of(context). showSnackBar(SnackBar(
                          content: Text("Carregando..."),
                          duration: Duration(seconds: 2),
                          backgroundColor: Colors.green,

                        ));


                    try{
                       user=await firebaseAuth.createUserWithEmailAndPassword(email: emailController.text,password: senhaController.text);
                    }
                    catch(e){
                       Scaffold.of(context). showSnackBar(SnackBar(
                          content: Text("Erro ! Revise os campos"),
                          duration: Duration(seconds: 2),
                          backgroundColor: Colors.green,

                        ));
                    }
                  
                   if(user!=null){
                     idade=double.tryParse(idadeController.text,);
                     Usuario usuario = Usuario(emailController.text,nomeController.text,sexo,idade,'',user.uid);
                     UsuarioSing usu = UsuarioSing(usuario: usuario);
                     await  UsuarioDAO.addUsuario(usuario);
                   Navigator.of(context).pushAndRemoveUntil(
                                        MaterialPageRoute(
                                          builder: (context) => MainPage()),
                                        ((Route<dynamic> route) => false));
                  
                   }
                  }
                },
              )
            ],
          ),
          )
        ),
      )
        ],
      ),
      )
    );
  }
}