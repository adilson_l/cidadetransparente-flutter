import 'dart:io';
import 'package:cidadetransparente/classes/problema.dart';
import 'package:cidadetransparente/singletons/usuarioSingleton.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:cidadetransparente/components/textfieldpadrao.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CadastrarProblemaPage extends StatefulWidget {
  @override
  _CadastrarProblemaPageState createState() => _CadastrarProblemaPageState();
}

class _CadastrarProblemaPageState extends State<CadastrarProblemaPage> {
  TextEditingController descricaoController= TextEditingController();
  TextEditingController enderecoController= TextEditingController();

  UsuarioSing usu = UsuarioSing();
  String tipo;
  List<String> links= List();
  List<File> imagens = List();
  List<String> listatipos = [
    'Água e Esgoto',
    'Árvores Caídas',
    'Assaltos',
    'Bueiros entupidos',
    'Iluminação Pública',
    'Semáforos Queimados'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cadastrar Problema"),
        elevation: 0,
      ),
      body:Stack(
        children: <Widget>[
           Container(
              height:  MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Colors.green[200],
                    Colors.green
                  ],
                ),
              ),
            ),
             SingleChildScrollView(
        child: Form(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child:Column(
            children: <Widget>[
             Center(
               child:  Text("Selecione o tipo de problema",style: TextStyle(color: Colors.white,fontSize: 16),),
             ),
                           Padding(padding: EdgeInsets.symmetric(vertical: 8),),

               DropdownButton(
              value: tipo,
              hint: Text("Selecione aqui",style: TextStyle(color: Colors.white),),
              items: listatipos.map((String m) {
                return new DropdownMenuItem<String>(
                  value: m,
                  child: Text(m),
                );
              }).toList(),
              onChanged: (String novo) {
                tipo = novo;
                setState(() {});
              },
            ),            
              Padding(padding: EdgeInsets.symmetric(vertical: 8),),
              

             TextFieldPadrao(
                labelText: "Descrição",
                controller: descricaoController,
                maxLines: 5,
                validator: (value){
                  if(value==null || value.isEmpty)
                    return "Campo obrigátorio";
                },
              ),
              Padding(padding: EdgeInsets.symmetric(vertical: 8),),
             TextFieldPadrao(
                labelText: "Endereço",
                controller: enderecoController,
                validator: (value){
                  if(value==null || value.isEmpty)
                    return "Campo obrigátorio";
                },
              ),
              Padding(padding: EdgeInsets.symmetric(vertical: 8),),
               Center(
               child:  Text("Fotos",style: TextStyle(color: Colors.white,fontSize: 16),),
             ),
              GridView.builder(
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 8,
                  mainAxisSpacing: 8,
                ),
                itemCount: imagens.length,
                itemBuilder: (context,i){
                  return Image.file(imagens[i]);
                },
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton( 
                child: Icon(Icons.add_a_photo),
                shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                onPressed: ()async{
                  var image = await ImagePicker.pickImage(source: ImageSource.camera);
                  if(image!=null)
                    imagens.add(image);
                  setState(() {});
                },
              ),
              RaisedButton(
                child: Icon(Icons.collections),
                shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                onPressed: ()async{
                  var image = await ImagePicker.pickImage(source: ImageSource.gallery);
                  if(image!=null)
                    imagens.add(image);
                  setState(() {});
                },
              )
                ],
              ),
              RaisedButton( 
                shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                child: Text("Finalizar"),
                onPressed: ()async{
                addProblema();
                Navigator.of(context).pop();
                },
              )
            ],
          ),
        ),
      )),
        ],
      )
      
     
    );
  }
  void addProblema()async{
      Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
                  for (File img in imagens) {
                    StorageUploadTask uploadTask =   FirebaseStorage.instance.ref().child("problemas/"+usu.usuario.id+"/"+DateTime.now().toIso8601String()).putFile(img);
                    StorageTaskSnapshot snap= await uploadTask.onComplete;
                    var link = await snap.ref.getDownloadURL();
                    links.add(link.toString());                    
                  }
                  Problemas problemas= Problemas(descricaoController.text, tipo, Timestamp.now(),
                   '', '', 'Aberto', usu.usuario.id, links,GeoPoint(position.latitude,position.longitude),enderecoController.text,'');
                   await Firestore.instance.collection("problemas").add(problemas.toMap());
  }
}

