import 'package:cidadetransparente/classes/usuario.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class UsuarioDAO{

    static Future<String> addUsuario(Usuario usuario) async{
      String retorno='falha';
        var doc = await Firestore.instance.collection('usuarios').add(usuario.toMap());
        if(doc!=null){
          retorno=doc.documentID;
        }

      return retorno;
    }

    static Future<Usuario> findUsuario(String uid) async{
      Usuario usu;
      var doc = await Firestore.instance.collection("usuarios").getDocuments();
      if(doc.documents.isNotEmpty){
        doc.documents.forEach((f){
          Usuario aux= Usuario.map(f.data,f.documentID);
          if(aux.uid==uid)
            usu=aux;
        });
      }
      return usu;
    }
}