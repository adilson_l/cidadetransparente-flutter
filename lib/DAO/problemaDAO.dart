
import 'package:cidadetransparente/classes/problema.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ProblemaDAO{
  static Future<List<Problemas> > buscarMarcadores()async{
    List<Problemas> problemas= List();
    var doc = await Firestore.instance.collection('problemas').getDocuments();

    if(doc.documents.isNotEmpty){
      doc.documents.forEach((f){
        problemas.add(Problemas.map(f.data,f.documentID));
      });

      problemas.forEach((f){
        
      });
    }

    return problemas;
  }

  static Future editProblema(Problemas problema)async{
    await  Firestore.instance.collection("problemas").document(problema.id).updateData(problema.toMap());
  }

}