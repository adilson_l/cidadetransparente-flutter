import 'package:flutter/material.dart';

class TextFieldPadrao extends TextFormField {
  TextFieldPadrao(
      {this.controller,
      this.style,
      this.labelText,
      this.labelStyle,
      this.enabled,
      this.initialValue,
      this.textAlign,
      this.keyboardType,
      this.textInputAction,
      this.validator,
      this.onFieldSubmitted,
      this.focusNode,
      this.filled,
      this.fillColor,
      this.maxLines,
      this.obscureText,
      this.suffix})
      :super(
        controller:controller,
        enabled:enabled,
        initialValue:initialValue,
        textAlign:textAlign ?? TextAlign.start,
        keyboardType:keyboardType,
        textInputAction:textInputAction,
        validator:validator,
        obscureText:obscureText ?? false,
        onFieldSubmitted:onFieldSubmitted,
        focusNode:focusNode,
        style:TextStyle(color: Colors.white),
        maxLines:maxLines,
        decoration: InputDecoration(
          suffixIcon: suffix ?? null,
        filled: filled,
        labelStyle: TextStyle(color: Colors.white),
        fillColor:fillColor,
        enabledBorder: OutlineInputBorder(borderSide:BorderSide(color: Colors.white) ),
        focusedBorder: OutlineInputBorder(borderSide:BorderSide(color: Colors.white)),
        labelText:labelText,
      )
      );
  final TextEditingController controller;
  final String labelText;
  final TextStyle labelStyle;
  final TextStyle style;
  final bool enabled;
  final String initialValue;
  final TextAlign textAlign;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onFieldSubmitted;
  final FocusNode focusNode;
  final bool filled;
  final Color fillColor;
  final int maxLines;
  final bool obscureText;
  final Widget suffix;
}
