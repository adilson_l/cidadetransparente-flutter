import 'package:cidadetransparente/DAO/problemaDAO.dart';
import 'package:cidadetransparente/classes/problema.dart';
import 'package:flutter/material.dart';

class AlteraEstadoProblema extends StatefulWidget {
  AlteraEstadoProblema(this.problema);
  final Problemas problema;
  @override
  _AlteraEstadoProblemaState createState() => _AlteraEstadoProblemaState();
}

class _AlteraEstadoProblemaState extends State<AlteraEstadoProblema> {
  TextEditingController solucaoController= TextEditingController();
  bool visivel=false;
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.lightGreen,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8),
          ),
          RaisedButton(
             shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Text("Concluir"),
            onPressed: (){
              setState(() {
               visivel=!visivel; 
              });
            },
          ), Padding(
            padding: EdgeInsets.symmetric(vertical: 8),
          ),
            RaisedButton(
               shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Text("Em andamento"),
            onPressed: (){
              widget.problema.status="Em andamento";
              ProblemaDAO.editProblema(widget.problema);
              Navigator.of(context).pop();
            },
          ) ,
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8),
          ),
          !visivel ? Text('')
          :Column(
            children: <Widget>[
               TextField( 
            maxLines: 3,
            controller:solucaoController,
            decoration: InputDecoration(
              hintText: "Comente sobre a solução"
            ),
          ),
          RaisedButton(
            child: Text("Finalizar"),
             shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            onPressed: (){
              widget.problema.status="Solucionado";
              widget.problema.solucao=solucaoController.text;
              ProblemaDAO.editProblema(widget.problema);
              Navigator.of(context).pop();            },
          )
            ],
          )

        ],
    ));
  }
}