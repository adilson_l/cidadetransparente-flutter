import 'package:cidadetransparente/main.dart';
import 'package:cidadetransparente/pages/agencias.dart';
import 'package:cidadetransparente/pages/cadastroProb.dart';
import 'package:cidadetransparente/pages/listarProblemas.dart';
import 'package:cidadetransparente/pages/meuPerfil.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class Gaveta extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Icon(Icons.place,size: MediaQuery.of(context).size.height*0.2,color: Colors.green,),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Meu Perfil"),
               onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder:(context)=>MeuPerfilPage()));
              },
            ),
            ListTile(
              leading: Icon(Icons.phone),
              title: Text("Agências"),
               onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder:(context)=>AgenciaPage()));
              },
            ),
            ListTile(
              leading: Icon(Icons.edit),
              title: Text("Cadastrar Problema"),
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder:(context) =>CadastrarProblemaPage()));
              },
            ),
            ListTile(
              leading: Icon(Icons.list),
              title: Text("Listar problema"),
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder:(context)=>ListarProblemaPage()));
              },
            ),
              ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text("Sair"),
              onTap: () async{
               MyHomePageState.userglobal=null;
               MyHomePageState.logadoglobal=false;
               await FirebaseAuth.instance.signOut();
                 Navigator.of(context).pushAndRemoveUntil(
                                        MaterialPageRoute(
                                          builder: (context) => MyHomePage()),
                                        ((Route<dynamic> route) => false));
              },
            )
          ],
        ),
      ),
    );
  }
}