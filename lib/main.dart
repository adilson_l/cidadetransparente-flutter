import 'package:cidadetransparente/DAO/usuarioDAO.dart';
import 'package:cidadetransparente/classes/usuario.dart';
import 'package:cidadetransparente/pages/cadastroUsu.dart';
import 'package:cidadetransparente/pages/mainPage.dart';
import 'package:cidadetransparente/singletons/usuarioSingleton.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
 bool logado=false;
  FirebaseUser user_global;

void main() async{
 
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  user_global = await firebaseAuth.currentUser();
  if(user_global==null)
    print('não logado');
  else
    logado=true;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Cidade transparente',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  MyHomePageState createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> {
  
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  TextEditingController emailController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController senhaController = TextEditingController();
  static FirebaseUser userglobal=user_global;
  bool versenha= false;
  Key scafold=new Key('scafold');
  static bool logadoglobal=logado;
  @override
  void initState() {
    if(logadoglobal){
      UsuarioDAO.findUsuario(userglobal.uid).then((usu){
        UsuarioSing usuarioSing = UsuarioSing(usuario: usu);
            if(usuarioSing.usuario!=null){
                          Navigator.of(context).pushAndRemoveUntil(
                                        MaterialPageRoute(
                                          builder: (context) => MainPage()),
                                        ((Route<dynamic> route) => false));
                  }
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key:scafold ,
      appBar: AppBar(
        title: Text("Cidade Transparente"),
        centerTitle: true,
      elevation: 0,
      ),
      body:Builder(
        
        builder: (context)=>Stack(
        children: <Widget>[
          Container(
              height:  MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Colors.green[200],
                    Colors.green
                  ],
                ),
              ),
            ),
            Center(
          child:SingleChildScrollView(
            child: Form(
            key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    style: TextStyle(color: Colors.white),
                    controller: emailController,
                     validator: (newValue){
                   if(newValue==null || newValue.isEmpty)
                    return "Campo obrigatório";
                 },
                    decoration: InputDecoration(
                        labelText: "Email",
                        labelStyle: TextStyle(color: Colors.white),
                        enabledBorder: OutlineInputBorder(borderSide:BorderSide(color: Colors.white) ),
                        focusedBorder: OutlineInputBorder(borderSide:BorderSide(color: Colors.white))),
                  ),
                )),
            Container(
              width: MediaQuery.of(context).size.width * 0.8,
              child: TextFormField(
                
                obscureText: versenha,
                controller: senhaController,
                 style: TextStyle(color: Colors.white),
                 validator: (newValue){
                   if(newValue==null || newValue.isEmpty || newValue.length < 6)
                    return "Campo obrigatório";
                 },
                decoration: InputDecoration(
                    suffixIcon: IconButton(
                      icon: Icon( versenha ?Icons.visibility: Icons.visibility_off,color: Colors.white,),
                      onPressed: (){
                        setState(() {
                         versenha=!versenha; 
                        });
                      },
                      ),
                    labelText: "Senha",
                    labelStyle: TextStyle(color: Colors.white),
                    focusColor: Colors.green,
                    enabledBorder: OutlineInputBorder(borderSide:BorderSide(color: Colors.white) ),
                    focusedBorder: OutlineInputBorder(borderSide:BorderSide(color: Colors.white))),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlatButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                  splashColor: Colors.green,
                  child: Text("Registrar",style: TextStyle(color: Colors.white)),
                  onPressed: (){
                    Navigator.of(context).push(MaterialPageRoute(builder:(context)=>CadastroUsuarioPage()));
                  },
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                  splashColor: Colors.green,
                  child: Text("Login",style: TextStyle(color: Colors.white),),
                  color: Colors.lightGreen,
                  onPressed: ()async{
                    if(_formKey.currentState.validate())
                    {
                      Scaffold.of(context). showSnackBar(SnackBar(
                          content: Text("Carregando..."),
                          duration: Duration(seconds: 2),
                          backgroundColor: Colors.green,

                        ));
                      FirebaseUser fire;
                      try{
                       fire= await firebaseAuth.signInWithEmailAndPassword(email: emailController.text,password: senhaController.text);
                      }catch(e){
                       Scaffold.of(context). showSnackBar(SnackBar(
                          content: Text("Erro ! Revise os campos"),
                          duration: Duration(seconds: 2),
                          backgroundColor: Colors.green,

                        ));
                      }
                      if(fire!=null){
                       Usuario aux=await UsuarioDAO.findUsuario(fire.uid);
                       UsuarioSing usuarioSing = UsuarioSing(usuario: aux);
                       if(usuarioSing.usuario!=null){
                          Navigator.of(context).pushAndRemoveUntil(
                                        MaterialPageRoute(
                                          builder: (context) => MainPage()),
                                        ((Route<dynamic> route) => false));
                  }
                       }
                      }
                    
                  /*  Navigator.of(context).pushAndRemoveUntil(
                                        MaterialPageRoute(
                                          builder: (context) => MainPage()),
                                        ((Route<dynamic> route) => false));
                  }, */
                  }
                )
              ],
            ),
            Divider(),
         
          ],
        ),
      ) ,
          )),
        ],
      ) ,
      )
    );
  }
}
